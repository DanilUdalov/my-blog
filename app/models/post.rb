class Post < ActiveRecord::Base
  validates :title, presence: true, length: { minimum: 5 }
  validates :body,  presence: true

  has_many :comments, dependent: :destroy
  has_many :favourite_posts
  belongs_to :user
  has_and_belongs_to_many :categories

  mount_uploader :image, ImageUploader

  self.per_page = 6
end
