class User < ActiveRecord::Base
  has_secure_password

  validates_uniqueness_of :email
  validates_presence_of :email
  validates_presence_of :password, on: :create

  has_many :comments, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :favourite_posts, dependent: :destroy
  has_many :accounts, dependent: :destroy

  mount_uploader :avatar, AvatarUploader
  self.per_page = 10

  def full_name
    "#{name} aka #{lastname}"
  end
end
