#
class Admin::PostsController < AdminsController
  def index
    @posts = Post.all
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
  end
end
