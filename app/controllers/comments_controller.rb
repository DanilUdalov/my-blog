class CommentsController < ApplicationController
  before_filter :authorize, only: [:create, :destroy]
  before_action :current_comment

  def create
    @comment = @post.comments.create(comment_params)
    @comment.user_id = current_user.id
    @comment.save
    redirect_to post_path(@post)
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

  def current_comment
    @post = Post.find(params[:post_id])
  end
end
