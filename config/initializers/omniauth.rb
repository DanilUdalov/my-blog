Rails.application.config.middleware.use OmniAuth::Builder do
  provider :vkontakte, ENV['vk_key'], ENV['vk_secret'], scope: 'email'
  provider :facebook, ENV['fc_key'], ENV['fc_secret'], scope: 'email'
end