require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:user) }

  describe 'associations' do
    it { have_many(:accounts).dependent(:destroy) }
    it { have_many(:comments).dependent(:destroy) }
    it { have_many(:favourite_posts).dependent(:destroy) }
    it { have_many(:posts).dependent(:destroy) }
  end

  describe 'validations' do
    it { have_secure_token }
    it { have_secure_password }
    it { validate_uniqueness_of(:email) }
    it { validate_presence_of(:email) }
    it { validate_presence_of(:password).on(:create) }
  end

  describe '#full_name' do
    subject { FactoryGirl.create(:user) }
    it 'returns fullname' do
      expect(subject.full_name).to eq("#{user.name} aka #{user.lastname}")
    end
  end
end
