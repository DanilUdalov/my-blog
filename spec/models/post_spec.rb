require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:post) { FactoryGirl.build(:post, user: user) }

  describe 'associations' do
    it { belong_to(:user) }
    it { have_and_belong_to_many(:categories) }
    it { have_many(:comments) }
    it { have_many(:favourite_posts) }
  end

  describe 'validations' do
    it { validate_presence_of(:body) }
    it { validate_presence_of(:title) }
    it { validate_length_of(:title).is_at_least(5) }
  end
end
