require 'rails_helper'

RSpec.describe FavouritePost, type: :model do
  describe 'associations' do
    it { belong_to(:post) }
    it { belong_to(:user) }
  end

  describe '.manage' do
    context 'when object is exist' do
      let!(:favourite_post) { FactoryGirl.create(:favourite_post) }

      it 'destroys object' do
        expect {
          described_class.manage(favourite_post.post)
        }.to change { described_class.count }.from(1).to(0)
      end
    end

    context 'when object is new' do
      let!(:post) { FactoryGirl.create(:post) }
      let(:user) { FactoryGirl.create(:user) }

      it 'creates object' do
        expect {
          user.favourite_posts.manage(post)
        }.to change { described_class.count }.from(0).to(1)
      end
    end    
  end
end
