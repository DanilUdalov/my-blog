require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:post) { FactoryGirl.build(:post, user: user) }
  let(:comment) { FactoryGirl.build(:comment, post: post, user: user) }

  describe 'associations' do
    it { belong_to(:post) }
    it { belong_to(:user) }
  end
end
