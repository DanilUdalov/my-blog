FactoryGirl.define do
  factory :comment do
    body Faker::Lorem.sentences
    user
    post
  end
end
