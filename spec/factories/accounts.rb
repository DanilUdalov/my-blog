FactoryGirl.define do
  factory :account do
    uid { Faker::Number.number(21) }
    provider 'facebook'
    email { Faker::Internet.email }
    user nil
  end
end
