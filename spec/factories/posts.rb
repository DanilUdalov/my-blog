FactoryGirl.define do
  factory :post do
    title Faker::Lorem.words
    body Faker::Lorem.sentences
    user
  end

  trait :with_user do
    user
  end
end
