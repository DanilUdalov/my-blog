require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user) }
  before { session[:user_id] = user.id }

  subject { response }

  describe '#index' do
    let!(:post) { FactoryGirl.create(:post) }

    before { get :index }

    it 'assigns posts' do
      expect(assigns(:posts)).to eq([post])
    end

    it 'status success' do
      is_expected.to have_http_status(200)
    end
  end

  describe '#new' do
    before { get :new }

    it { expect(assigns(:post)).to be_a_new(Post) }
    it { is_expected.to have_http_status :ok }
    it { is_expected.to render_template(:new) }
  end

  describe '#show' do
    let(:post) { FactoryGirl.create(:post)  }

    before { get :show, id: post.id }

    it { is_expected.to render_template(:show) }
    it { expect(assigns(:post).title).to eq(post.title) }
    it { expect(assigns(:post).body).to eq(post.body) }
  end

  describe '#create' do
    context 'valid data' do
      let(:post) { FactoryGirl.create(:post) }
      before { FactoryGirl.create(:post) }

      it { is_expected.to have_http_status(200) }
    end

    context 'invalid data' do
      before { post :create, post: { title: nil, body: nil } }

      it { expect(response).to render_template(:new) }
    end
  end

  describe '#update' do
    let!(:post) { FactoryGirl.create(:post) }

    context 'valid data' do
      before do
        put :update, id: post.id, post: { title: 'new title' }
        post.reload
      end

      # it { expect(assigns(:post).title).to eq('new title') }
      # it { is_expected.to redirect_to(post_path(post.id)) }
    end

    context 'invalid data' do
      before do
        put :update, id: post.id, post: { title: nil }
        post.reload
      end

      # it { is_expected.to render_template(:edit) }
    end
  end

  describe '#destroy' do
    let!(:post) { FactoryGirl.create(:post) }
    before { delete :destroy, id: post.id }

    it { is_expected.to redirect_to root_path }
  end

  describe '#favourite' do
    let!(:post) { FactoryGirl.create(:post) }
    let!(:favourite_post) { FactoryGirl.create(:favourite_post) }
    before { patch :favourite, id: post.id }
  end
end
