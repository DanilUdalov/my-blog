require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  subject { response }

  describe '#index' do
    before { get :index }

    it 'be success' do
      expect(response).to have_http_status(200)
    end

    it 'render template' do
      expect(response).to render_template('index')
    end
  end

  describe '#new' do
    before { get :new }

    it 'assign user' do
      expect(assigns(:user)).to be_a_new(User)
    end

    it 'be success' do
      is_expected.to be_success
    end

    it 'render template' do
      is_expected.to render_template(:new)
    end
  end

  describe '#create' do
    context 'valid data' do
      before do
        post :create, user:
          {
            email: 'some@mail.ru',
            password: 'qazwsx123',
            password_confirmation: 'qazwsx123'
          }
      end

      it 'be created' do
        expect(User.last.email).to eq('some@mail.ru')
      end

      it 'be found' do
        is_expected.to have_http_status :redirect
      end

      it 'has session' do
        expect(session[:user_id]).to be_eql(User.last.id)
      end

      it 'redirect to root path' do
        expect(response).to redirect_to(root_path)
      end
    end

    context 'invalid data' do
      before do
        post :create, user:
          {
            email: '',
            password: 'qazwsx123',
            password_confirmation: 'qazwsx123'
          }
      end
      it 'render template' do
        expect(response).to render_template(:new)
      end
    end
  end

  describe '#show' do
    let(:user) { FactoryGirl.create(:user) }

    before { get :show, id: user.id }

    it 'be success' do
      is_expected.to have_http_status(200)
    end

    it 'render template' do
      is_expected.to render_template(:show)
    end
  end

  describe '#edit' do
    let(:user) { FactoryGirl.create(:user) }

    before { get :edit, id: user.id }

    it 'render template' do
      is_expected.to render_template('edit')
    end

    it 'be success' do
      is_expected.to be_succes
    end
  end

  describe '#update' do
    let!(:user) { FactoryGirl.create(:user) }

    context 'valid data' do
      before do
        put :update, id: user.id, user: { password: '222222', password_confirmation: '222222' }
        user.reload
      end

      it 'redirect_to user page' do
        is_expected.to redirect_to(user_path(user.id))
      end
    end

    context 'invalid data' do
      before do
        put :update, id: user.id, user: { password: '222222', password_confirmation: '222221' }
        user.reload
      end

      it 'render template' do
        is_expected.to render_template(:edit)
      end
    end
  end

  describe '#change_permission' do
    before(:each) do
      request.env['HTTP_REFERER'] = 'posts/'
    end

    before { get :change_permission, id: user.id }

    let(:user) { FactoryGirl.create(:user) }

    it 'has change status' do
      user.reload
      expect(user.admin).to be_falsey
    end

    it 'has not permission' do
      expect(user.admin).to be_truthy
    end
  end

  describe '#destroy' do
    before(:each) do
      request.env['HTTP_REFERER'] = 'posts/'
    end

    let!(:user) { FactoryGirl.create(:user) }

    it 'destroy user' do
      expect { delete :destroy, id: user.id }.to change(User, :count).by(-1)
    end
  end
end
